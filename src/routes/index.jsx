import MainLayout from '@layouts/MainLayout';

import Home from '@pages/Home';
import NotFound from '@pages/NotFound';
import advice from '@pages/Advice/index';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: advice,
    // layout: MainLayout,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
