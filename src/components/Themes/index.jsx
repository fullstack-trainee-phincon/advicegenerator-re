import React from 'react';
import PropTypes from 'prop-types';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { selectTheme } from '@containers/App/selectors';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { setTheme } from '@containers/App/actions';

const Themes = ({ theme, children }) => {
  const isLight = theme === 'light';

  const myTheme = createTheme({
    palette: {
      mode: isLight ? 'light' : 'dark',
    },
  });

  return <ThemeProvider theme={myTheme}>{children}</ThemeProvider>;
};

Themes.propTypes = {
  theme: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
});

const mapDispatchToProps = {
  setTheme,
};

export default connect(mapStateToProps, mapDispatchToProps)(Themes);
