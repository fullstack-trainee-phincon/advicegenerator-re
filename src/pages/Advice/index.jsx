import React from 'react';
import classes from './style.module.scss';
import dice from '@static/images/dice.png';
import { useDispatch, useSelector } from 'react-redux';
import { getAdvice, setTheme } from '@containers/App/actions';
import LightModeIcon from '@mui/icons-material/LightMode';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import Themes from '@components/Themes/index';

const Advice = () => {
  const dispatch = useDispatch();
  const adviceData = useSelector((state) => state.app.advice);
  const theme = useSelector((state) => state.app.theme);

  const refreshAdvice = () => {
    dispatch(getAdvice());
  };

  const handleThemeToggle = () => {
    dispatch(setTheme(theme === 'light' ? 'dark' : 'light'));
  };

  React.useEffect(() => {
    dispatch(getAdvice());
  }, [dispatch]);

  const { id, advice } = adviceData || {};

  return (
    <Themes>
      <div className={classes.Box}>
        <div className={`${classes.Card} ${theme === 'light' ? classes.Card_DarkMode : ''}`}>
          <div className={classes.themeSwitcher} onClick={handleThemeToggle}>
            {theme === 'light' ? <NightsStayIcon /> : <LightModeIcon />}
          </div>
          <div className={classes.cardTitle}>{id ? `Advice #${id}` : 'Loading...'}</div>
          <div className={classes.cardContent}>{advice || 'No advice available'}</div>
          <div className={classes.cardFooter}>
            <hr className={classes.leftLine} />
            <hr className={classes.upLeftLine} />
            <hr className={classes.upRightLine} />
            <hr className={classes.rightLine} />
          </div>
          <div className={classes.button} onClick={refreshAdvice}></div>
          <div className={classes.dice} onClick={refreshAdvice}>
            <img src={dice} alt="Dice" />
          </div>
        </div>
      </div>
    </Themes>
  );
};

export default Advice;
