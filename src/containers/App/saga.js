import { GET_ADVICE } from './constants';
import { setAdvice } from './actions';
import { getRandomAdvice } from '@domain/api';
import { call, put, takeLatest } from 'redux-saga/effects';

export function* doGetRandomAdvice() {
  try {
    const response = yield call(getRandomAdvice);
    yield put(setAdvice(response.slip));
  } catch (error) {
    console.log(error);
  }
}

export default function* appSaga() {
  yield takeLatest(GET_ADVICE, doGetRandomAdvice);
}
