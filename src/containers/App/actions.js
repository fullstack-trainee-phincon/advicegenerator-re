import { SET_LOCAL, SET_THEME, SET_ADVICE, GET_ADVICE } from '@containers/App/constants';

export const setLocale = (locale) => ({
  type: SET_LOCAL,
  locale,
});

export const setTheme = (theme) => ({
  type: SET_THEME,
  theme,
});

export const setAdvice = (advice) => ({
  type: SET_ADVICE,
  advice,
});
export const getAdvice = () => ({
  type: GET_ADVICE,
});
